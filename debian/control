Source: winmbf
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Fabian Greffrath <fabian@debian.org>
Build-Depends:
 debhelper (>= 9),
 dh-autoreconf,
 libsdl-mixer1.2-dev,
 libsdl-net1.2-dev,
 libsdl1.2-dev,
 pkg-config
Standards-Version: 3.9.3
Homepage: http://mancubus.net/svn/winmbf/
Vcs-Git: git://anonscm.debian.org/pkg-games/winmbf.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-games/winmbf.git

Package: winmbf
Architecture: any-i386
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 freedoom | game-data-packager
Provides:
 boom-engine,
 doom-engine
Description: faithful Doom clone with minor enhancements
 Doom is a 3d shoot'em'up game, written by id Software, first
 released in 1993.
 .
 WinMBF is a source port project by Team Eternity. It is a direct
 port of the MBF port by Lee Killough to operating systems that
 support the SDL library. It removes the original low-level system
 code and replaces it with a modification of the code used by the
 Eternity Engine. Changes in WinMBF include numerous bug fixes to
 fatal errors that were in contained in the Boom and MBF codebases,
 support for windowed and fullscreen video modes, ability to adjust
 the zone heap size, and improved behavior of the sound engine
 including a higher sample rate. Gameplay problems in the ancestral
 ports are not addressed, in order to ensure 100% demo compatibility
 is retained.
 .
 WinMBF is based on MBF, which in turn is based on Boom,
 a freely available port of Doom for DOS, written by
 TeamTNT (http://www.teamtnt.com/). Unlike MBF, WinMBF is available
 only under the terms of the GPL, since it was based on the
 GPL-patched MBF source and additionally contains code from
 Eternity, SMMU, PrBoom, and Chocolate Doom.
 .
 WinMBF requires an IWAD to play. A free IWAD is available
 in the freedoom package. You can install your commercial
 Doom IWADs using game-data-packager.
